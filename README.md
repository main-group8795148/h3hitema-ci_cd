# Build Dockerfile server

```
docker build -t my-server .
docker run -p 3000:3000 --name my-server my-server
```

# Build Dockerfile CLient

```
docker build -t my-client .
docker run -p 5173:5173 --name my-client my-client
```
