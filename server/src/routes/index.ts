import Controller from '../controllers/Controller';

const routes = async (app, options) => {
	app.get('/data', Controller.findMany);
};

export default routes;
