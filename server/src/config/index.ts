import path from 'path';

export const PUBLIC_PATH = path.join(__dirname, '..', 'uploads');
