import { Prisma } from '@prisma/client';
import prisma from '../../prisma/index';
import _ from 'lodash';

class Controller {
	async findMany(request, reply) {
		return await prisma.myData.findMany({
			orderBy: {
				id: 'asc',
			},
		});
	}
}

export default new Controller();
