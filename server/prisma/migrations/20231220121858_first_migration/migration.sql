/*
  Warnings:

  - Changed the type of `annee_publication` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `code_region` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `nombre_d_habitants` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `densite_de_population_au_km2` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `variation_de_la_population_sur_10_ans_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `dont_contribution_du_solde_naturel_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `dont_contribution_du_solde_migratoire_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `population_de_moins_de_20_ans` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `population_de_60_ans_et_plus` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `taux_de_chomage_au_t4_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `taux_de_pauvrete_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `nombre_de_logements` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `nombre_de_residences_principales` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `taux_de_logements_sociaux_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `taux_de_logements_vacants_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `taux_de_logements_individuels_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `moyenne_annuelle_de_la_construction_neuve_sur_10_ans` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `construction` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_nombre_de_logements` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_logements_mis_en_location` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_logements_demolis` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_taux_de_logements_vacants_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_taux_de_logements_individuels_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_loyer_moyen_en_eur_m2_mois` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_age_moyen_du_parc_en_annees` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `parc_social_taux_de_logements_energivores_e_f_g_en` on the `myData` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "myData" DROP COLUMN "annee_publication",
ADD COLUMN     "annee_publication" INTEGER NOT NULL,
DROP COLUMN "code_region",
ADD COLUMN     "code_region" INTEGER NOT NULL,
DROP COLUMN "nombre_d_habitants",
ADD COLUMN     "nombre_d_habitants" INTEGER NOT NULL,
DROP COLUMN "densite_de_population_au_km2",
ADD COLUMN     "densite_de_population_au_km2" INTEGER NOT NULL,
DROP COLUMN "variation_de_la_population_sur_10_ans_en",
ADD COLUMN     "variation_de_la_population_sur_10_ans_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "dont_contribution_du_solde_naturel_en",
ADD COLUMN     "dont_contribution_du_solde_naturel_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "dont_contribution_du_solde_migratoire_en",
ADD COLUMN     "dont_contribution_du_solde_migratoire_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "population_de_moins_de_20_ans",
ADD COLUMN     "population_de_moins_de_20_ans" INTEGER NOT NULL,
DROP COLUMN "population_de_60_ans_et_plus",
ADD COLUMN     "population_de_60_ans_et_plus" INTEGER NOT NULL,
DROP COLUMN "taux_de_chomage_au_t4_en",
ADD COLUMN     "taux_de_chomage_au_t4_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "taux_de_pauvrete_en",
ADD COLUMN     "taux_de_pauvrete_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "nombre_de_logements",
ADD COLUMN     "nombre_de_logements" INTEGER NOT NULL,
DROP COLUMN "nombre_de_residences_principales",
ADD COLUMN     "nombre_de_residences_principales" INTEGER NOT NULL,
DROP COLUMN "taux_de_logements_sociaux_en",
ADD COLUMN     "taux_de_logements_sociaux_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "taux_de_logements_vacants_en",
ADD COLUMN     "taux_de_logements_vacants_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "taux_de_logements_individuels_en",
ADD COLUMN     "taux_de_logements_individuels_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "moyenne_annuelle_de_la_construction_neuve_sur_10_ans",
ADD COLUMN     "moyenne_annuelle_de_la_construction_neuve_sur_10_ans" INTEGER NOT NULL,
DROP COLUMN "construction",
ADD COLUMN     "construction" INTEGER NOT NULL,
DROP COLUMN "parc_social_nombre_de_logements",
ADD COLUMN     "parc_social_nombre_de_logements" INTEGER NOT NULL,
DROP COLUMN "parc_social_logements_mis_en_location",
ADD COLUMN     "parc_social_logements_mis_en_location" INTEGER NOT NULL,
DROP COLUMN "parc_social_logements_demolis",
ADD COLUMN     "parc_social_logements_demolis" INTEGER NOT NULL,
DROP COLUMN "parc_social_taux_de_logements_vacants_en",
ADD COLUMN     "parc_social_taux_de_logements_vacants_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "parc_social_taux_de_logements_individuels_en",
ADD COLUMN     "parc_social_taux_de_logements_individuels_en" DOUBLE PRECISION NOT NULL,
DROP COLUMN "parc_social_loyer_moyen_en_eur_m2_mois",
ADD COLUMN     "parc_social_loyer_moyen_en_eur_m2_mois" DOUBLE PRECISION NOT NULL,
DROP COLUMN "parc_social_age_moyen_du_parc_en_annees",
ADD COLUMN     "parc_social_age_moyen_du_parc_en_annees" INTEGER NOT NULL,
DROP COLUMN "parc_social_taux_de_logements_energivores_e_f_g_en",
ADD COLUMN     "parc_social_taux_de_logements_energivores_e_f_g_en" INTEGER NOT NULL;
