import { IData } from '../models/Data';
import axiosClient from '../utils/axiosClient';

class Data {
	async findMany(): Promise<IData[]> {
		const request = await axiosClient.get('/data');
		const data = request.data?.filter((d: IData) => d.nom_region !== '');
		return data;
	}
}

export default new Data();
