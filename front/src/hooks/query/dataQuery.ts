import { useQuery } from 'react-query';
import Data from '../../services/Data';

export const useFindAllDataQuery = () => {
	return useQuery({
		queryKey: 'data',
		queryFn: Data.findMany,
	});
};
