import _ from 'lodash';
import { useFindAllDataQuery } from '../hooks/query/dataQuery';
import { IData } from '../models/Data';
import { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { TextField, Autocomplete } from '@mui/material';
import {
	Chart as ChartJS,
	CategoryScale,
	LinearScale,
	BarElement,
	Title,
	Tooltip,
	Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

const buildColumns = (data: IData[]) => {
	if (!data?.length) return [];
	const columns = [];
	for (const key in data[0]) {
		columns.push({
			name: _.capitalize(key).replaceAll('_', ' '),
			selector: key,
			sortable: true,
		});
	}

	console.log({ columns });
	return columns;
};

const groupNbHabitantParRegion = (data: IData[]) => {
	const nbHabitantParRegion = _.chain(data)
		.cloneDeep()
		.groupBy('nom_region')
		.map((value, key) => ({
			nom_region: key,
			nombre_d_habitants: _.sumBy(value, 'nombre_d_habitants'),
			order: key,
		}))
		.orderBy('order')
		.value();

	return nbHabitantParRegion;
};

const groupNbTauxChomage = (data: IData[]) => {
	const taux_de_chomage_au_t4_en = _.chain(data)
		.cloneDeep()
		.groupBy('nom_region')
		.map((value, key) => ({
			nom_region: key,
			taux_de_chomage_au_t4_en: _.sumBy(value, 'taux_de_chomage_au_t4_en'),
			order: key,
		}))
		.orderBy('order')
		.value();

	return taux_de_chomage_au_t4_en;
};

const groupNbConstructionParRegion = (data: IData[]) => {
	const construction = _.chain(data)
		.cloneDeep()
		.groupBy('nom_region')
		.map((value, key) => ({
			nom_region: key,
			construction: _.sumBy(value, 'construction'),
			order: key,
		}))
		.orderBy('order')
		.value();

	return construction;
};

const Home = () => {
	const { data, isSuccess } = useFindAllDataQuery();
	const [columns, setColumns] = useState([]);
	const [allData, setAllData] = useState<IData[]>([]);
	const [regions, setRegions] = useState<string[]>([]);
	const [departements, setDepartements] = useState<string[]>([]);
	const [publishYear, setPublishYear] = useState<string[]>([]);
	const [codeDepartement, setCodeDepartement] = useState<string[]>([]);

	const [searchByRegion, setSearchByRegion] = useState<string>('');
	const [searchByDepartement, setSearchByDepartement] = useState<string>('');
	const [searchByCodeDepartement, setSearchByCodeDepartement] = useState<string>('');
	const [searchByPublishYear, setSearchByPublishYear] = useState<string>('');
	const [searchByConstruction, setSearchByConstruction] = useState<string>('');

	const [nbHabitantParRegion, setNbHabitantParRegion] = useState<any[]>([]);
	const [nbTauxChomage, setNbTauxChomage] = useState<any[]>([]);
	const [nbConstructionParRegion, setNbConstructionParRegion] = useState<any[]>([]);
	useEffect(() => {
		if (!isSuccess) return;

		setAllData(data);
		setColumns(buildColumns(data!));
		setRegions(_.chain(data).uniqBy('nom_region').map('nom_region').value());
		setDepartements(
			_.chain(data).uniqBy('nom_departement').map('nom_departement').value()
		);
		setPublishYear(
			_.chain(data)
				.uniqBy('annee_publication')
				.map('annee_publication')
				.filter((e) => e !== null)
				.orderBy()
				.value()
		);

		setCodeDepartement(
			_.chain(data)
				.uniqBy('code_departement')
				.map('code_departement')
				.filter((e) => e !== null)
				.orderBy()
				.value()
		);

		setNbHabitantParRegion(groupNbHabitantParRegion(data!));
		setNbTauxChomage(groupNbTauxChomage(data!));
		setNbConstructionParRegion(groupNbConstructionParRegion(data!));
	}, [isSuccess]);

	useEffect(() => {
		let filteredData = _.chain(data);

		if (!_.isEmpty(searchByRegion)) {
			filteredData = filteredData.filter({ nom_region: searchByRegion });
		}

		if (!_.isEmpty(searchByDepartement)) {
			filteredData = filteredData.filter({ nom_departement: searchByDepartement });
		}

		if (!_.isEmpty(searchByCodeDepartement)) {
			filteredData = filteredData.filter({ code_departement: searchByCodeDepartement });
		}
		if (_.isNumber(searchByPublishYear)) {
			console.log('after', { searchByPublishYear });

			filteredData = filteredData.filter({ annee_publication: searchByPublishYear });
		}

		if (!_.isEmpty(searchByConstruction)) {
			filteredData = filteredData.filter(
				(item) => parseInt(item?.construction!) >= parseInt(searchByConstruction)
			);
		}

		setAllData(filteredData.value());
		console.log('filteredData.value', filteredData.value());

		if (filteredData.value()?.length) {
			setNbHabitantParRegion(groupNbHabitantParRegion(filteredData.value()!));
			setNbTauxChomage(groupNbTauxChomage(filteredData.value()!));
			setNbConstructionParRegion(groupNbConstructionParRegion(filteredData.value()!));
		}
	}, [
		searchByRegion,
		searchByDepartement,
		searchByCodeDepartement,
		searchByPublishYear,
		searchByConstruction,
	]);

	console.log({ allData, columns, regions, departements });
	return (
		<div className='max-w-screen-xl mx-auto'>
			<div className=''>
				{data?.length !== 0 && isSuccess === true ? (
					<div className='flex flex-col'>
						<div className='flex mb-12 overflow-x-auto'>
							{/* nbre habitants/region */}
							<Bar
								options={{
									responsive: true,
									plugins: {
										legend: {
											position: 'top' as const,
										},
										title: {
											display: true,
											text: "Nombre d'habitant par région",
										},
									},
								}}
								data={{
									labels: nbHabitantParRegion.map((e) => e.nom_region),
									datasets: [
										{
											label: "Nombre d'habitant par région",
											data: nbHabitantParRegion.map(
												(e) => e.nombre_d_habitants
											),
											borderColor: Array.from({ length: 17 }, () => {
												const r = Math.floor(Math.random() * 256);
												const g = Math.floor(Math.random() * 256);
												const b = Math.floor(Math.random() * 256);
												const alpha = Math.random().toFixed(2);
												return `rgba(${r}, ${g}, ${b}, ${alpha})`;
											}),
											backgroundColor: Array.from({ length: 17 }, () => {
												const r = Math.floor(Math.random() * 256);
												const g = Math.floor(Math.random() * 256);
												const b = Math.floor(Math.random() * 256);
												const alpha = Math.random().toFixed(2);
												return `rgba(${r}, ${g}, ${b}, ${alpha})`;
											}),
											borderWidth: 1,
										},
									],
								}}
							/>

							{/* Taux de chomage par région */}
							<Bar
								options={{
									responsive: true,
									plugins: {
										legend: {
											position: 'top' as const,
										},
										title: {
											display: true,
											text: 'Taux de chomage par région',
										},
									},
								}}
								data={{
									labels: nbTauxChomage.map((e) => e.nom_region),
									datasets: [
										{
											label: 'Taux de chomage par région',
											data: nbTauxChomage.map(
												(e) => e.taux_de_chomage_au_t4_en
											),
											borderColor: Array.from({ length: 17 }, () => {
												const r = Math.floor(Math.random() * 256);
												const g = Math.floor(Math.random() * 256);
												const b = Math.floor(Math.random() * 256);
												const alpha = Math.random().toFixed(2);
												return `rgba(${r}, ${g}, ${b}, ${alpha})`;
											}),
											backgroundColor: Array.from({ length: 17 }, () => {
												const r = Math.floor(Math.random() * 256);
												const g = Math.floor(Math.random() * 256);
												const b = Math.floor(Math.random() * 256);
												const alpha = Math.random().toFixed(2);
												return `rgba(${r}, ${g}, ${b}, ${alpha})`;
											}),
											borderWidth: 1,
										},
									],
								}}
							/>
							{/* Nombre de construction par région */}
							<Bar
								options={{
									responsive: true,
									plugins: {
										legend: {
											position: 'top' as const,
										},
										title: {
											display: true,
											text: 'Nombre de construction par région',
										},
									},
								}}
								data={{
									labels: nbConstructionParRegion.map((e) => e.nom_region),
									datasets: [
										{
											label: 'Nombre de construction par région',
											data: nbConstructionParRegion.map(
												(e) => e.construction
											),
											borderColor: Array.from({ length: 17 }, () => {
												const r = Math.floor(Math.random() * 256);
												const g = Math.floor(Math.random() * 256);
												const b = Math.floor(Math.random() * 256);
												const alpha = Math.random().toFixed(2);
												return `rgba(${r}, ${g}, ${b}, ${alpha})`;
											}),
											backgroundColor: Array.from({ length: 17 }, () => {
												const r = Math.floor(Math.random() * 256);
												const g = Math.floor(Math.random() * 256);
												const b = Math.floor(Math.random() * 256);
												const alpha = Math.random().toFixed(2);
												return `rgba(${r}, ${g}, ${b}, ${alpha})`;
											}),
											borderWidth: 1,
										},
									],
								}}
							/>
						</div>

						<div className='flex gap-4 mb-4'>
							<Autocomplete
								disablePortal
								id='regions'
								options={regions}
								sx={{ width: 300 }}
								renderInput={(params) => (
									<TextField
										{...params}
										label='Regions'
									/>
								)}
								onChange={(e, value) => setSearchByRegion(value!)}
							/>
							<Autocomplete
								disablePortal
								id='departements'
								options={departements}
								sx={{ width: 300 }}
								renderInput={(params) => (
									<TextField
										{...params}
										label='Departements'
									/>
								)}
								onChange={(e, value) => setSearchByDepartement(value!)}
							/>
							<Autocomplete
								disablePortal
								id='code_departement'
								options={codeDepartement}
								sx={{ width: 300 }}
								renderInput={(params) => (
									<TextField
										{...params}
										label='Code Departement'
									/>
								)}
								onChange={(e, value) => setSearchByCodeDepartement(value!)}
							/>
							<Autocomplete
								disablePortal
								id='annee_publication'
								options={publishYear}
								sx={{ width: 300 }}
								getOptionLabel={(option) => (option ? String(option) : '')}
								renderInput={(params) => (
									<TextField
										{...params}
										label='Année publication'
									/>
								)}
								onChange={(e, value) => setSearchByPublishYear(value!)}
							/>

							<TextField
								id='construction'
								label='Construction'
								variant='outlined'
								type='number'
								onChange={(e) => setSearchByConstruction(e.target.value)}
							/>
						</div>

						<DataTable
							pagination
							columns={columns}
							data={allData}
						/>
					</div>
				) : (
					<h1>loading ...</h1>
				)}
			</div>
		</div>
	);
};

export default Home;
