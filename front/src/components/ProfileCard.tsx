import { Card, Dropdown } from 'flowbite-react';
import { IUser } from '../models/Data';
import { Link } from 'react-router-dom';
import { PUBLIC_PATH } from '../config';
import { useNavigate } from 'react-router-dom';

type ProfileCardProps = {
	user: IUser;
	deleteUser: (id: string) => void;
};

const ProfileCard = ({ user, deleteUser }: ProfileCardProps) => {
	const navigate = useNavigate();

	return (
		<Card className='max-w-sm cursor-pointer bg-red-500'>
			<div className='flex justify-end pt-2'>
				<Dropdown
					inline
					label=''
				>
					<Dropdown.Item>
						<Link
							to={`/edit-profile/${user.id}`}
							className='block px-4 py-1 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white'
						>
							Edit
						</Link>
					</Dropdown.Item>

					<Dropdown.Item>
						<span
							onClick={() => deleteUser(user.id)}
							className='block px-4 py-1 text-sm text-red-600 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white'
						>
							Delete
						</span>
					</Dropdown.Item>
				</Dropdown>
			</div>
			<div
				onClick={() => navigate(`/edit-profile/${user.id}`)}
				className='flex flex-col items-center pb-8'
			>
				<img
					alt='Bonnie image'
					height='96'
					src={PUBLIC_PATH + '/' + user.image}
					width='96'
					className='mb-3 rounded-full shadow-lg aspect-auto w-24 h-24 object-cover'
				/>
				<h5 className='mt-4 text-xl font-medium text-gray-900 dark:text-white'>
					{user.name}
				</h5>
				{/* <span className='text-sm text-gray-500 dark:text-gray-400'>{user.age} yo</span> */}
				{/* <span className='text-sm text-gray-500 text-bold'>{user.email}</span> */}
			</div>
		</Card>
	);
};

export default ProfileCard;
